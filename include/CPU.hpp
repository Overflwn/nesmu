#pragma once

using Byte = unsigned char;
using Word = unsigned short;
using u32 = unsigned int;

class Memory {
	public:
	
	static constexpr u32 MAX_MEM = 1024 * 64;
	private:


	public:
    Byte data[MAX_MEM];
	void initialize();

	//read 1 byte
	Byte& operator[](u32 addr)
	{
		//asset here addr < max_mem
		return this->data[addr];
	}

	//write 2 bytes
	void writeWord(Word val, u32 addr, u32& cycles);
};

class CPU6502 {
	private:
	Word PC;	//program counter
	Word SP;	//stack pointer



	Byte PS;	//status flags
	                // NV-BDIZC

    void LDASetStatus();
    void LDXSetStatus();
    void LDYSetStatus();
    void ANDSetStatus();

	public:
    Byte A, X, Y;	//registers
    //opcodes
    static constexpr Byte INS_LDA_IM = 0xA9; //load into A immediate
    static constexpr Byte INS_LDA_ZP = 0xA5; //load into A zeropage
    static constexpr Byte INS_LDA_ZPX = 0xB5; //load into A zeropage,x
    static constexpr Byte INS_LDA_AB = 0xAD; //load into A absolute
    static constexpr Byte INS_LDA_ABX = 0xBD; //load into A absolute,x
    static constexpr Byte INS_LDA_ABY = 0xB9; //load into A absolute,y
    static constexpr Byte INS_LDA_IxIdX = 0xA1; //load into A (indirect,x)
    static constexpr Byte INS_LDA_IdIxY = 0xB1; //load into A (indirect),y

    static constexpr Byte INS_LDX_IM = 0xA2; //load into X immediate
    static constexpr Byte INS_LDX_ZP = 0xA6; //load into X zeropage
    static constexpr Byte INS_LDX_ZPY = 0xB6; //load into X zeropage,y
    static constexpr Byte INS_LDX_AB = 0xAE; //load into X absolute
    static constexpr Byte INS_LDX_ABY = 0xBE; //load into X absolute,y

    static constexpr Byte INS_LDY_IM = 0xA0; //load into X immediate
    static constexpr Byte INS_LDY_ZP = 0xA4; //load into X zeropage
    static constexpr Byte INS_LDY_ZPX = 0xB4; //load into X zeropage,y
    static constexpr Byte INS_LDY_AB = 0xAC; //load into X absolute
    static constexpr Byte INS_LDY_ABX = 0xBC; //load into X absolute,y

    static constexpr Byte INS_STA_ZP = 0x85; //store accumulator zeropage
    static constexpr Byte INS_STA_ZPX = 0x95; //store accumulator zeropage,x
    static constexpr Byte INS_STA_AB = 0x8D; //store accumulator absolute
    static constexpr Byte INS_STA_ABX = 0x9D; //store accumulator absolute,x
    static constexpr Byte INS_STA_ABY = 0x99; //store accumulator absolute,y
    static constexpr Byte INS_STA_IxIdX = 0x81; //store accumulator (indirect,x)
    static constexpr Byte INS_STA_IdIxY = 0x91; //store accumulator (indirect),y

    static constexpr Byte INS_STX_ZP = 0x86; //store X zeropage
    static constexpr Byte INS_STX_ZPY = 0x96; //store X zeropage,y
    static constexpr Byte INS_STX_AB = 0x8E; //store X absolute

    static constexpr Byte INS_STY_ZP = 0x84; //store Y zeropage
    static constexpr Byte INS_STY_ZPX = 0x94; //store Y zeropage,x
    static constexpr Byte INS_STY_AB = 0x8C; //store Y absolute

    static constexpr Byte INS_TAX = 0xAA; //copy A to X
    static constexpr Byte INS_TAY = 0xA8; //copy A to Y
    static constexpr Byte INS_TXA = 0x8A; //copy X to A
    static constexpr Byte INS_TYA = 0x98; //copy Y to A

    static constexpr Byte INS_TSX = 0xBA; //copy SP to X
    static constexpr Byte INS_TXS = 0x9A; //copy X to SP
    static constexpr Byte INS_PHA = 0x48; //copy A to stack
    static constexpr Byte INS_PHP = 0x08; //copy PS to stack
    static constexpr Byte INS_PLA = 0x68; //pull val from stack into A
    static constexpr Byte INS_PLP = 0x28; //pull val from stack into PS

    static constexpr Byte INS_AND_IM = 0x29; //AND immediate
    static constexpr Byte INS_AND_ZP = 0x25; //AND zeropage
    static constexpr Byte INS_AND_ZPX = 0x35; //AND zeropage,x
    static constexpr Byte INS_AND_AB = 0x2D; //AND absolute
    static constexpr Byte INS_AND_ABX = 0x3D; //AND absolute,x
    static constexpr Byte INS_AND_ABY = 0x39; //AND absolute,y
    static constexpr Byte INS_AND_IxIdX = 0x21; //AND (indirect,x)
    static constexpr Byte INS_AND_IdIxY = 0x31; //AND (indirect),y

    static constexpr Byte INS_JSR = 0x20; //jump to subroutine, LITTLE BYTE FIRST

	CPU6502();
	~CPU6502();
	void reset(Memory& mem);
	void execute(Memory& mem, u32 cycles);
	Byte fetchByte(Memory& mem, u32& cycles);
	Word fetchWord(Memory& mem, u32& cycles);
	static Byte readByte(Memory& mem, Word address, u32& cycles);
	static Word readWord(Memory& mem, Word address, u32& cycles);
	static void writeByte(Memory& mem, Word address, u32& cycles, Byte data);
	static void writeWord(Memory& mem, Word address, u32& cycles, Byte data);
};
