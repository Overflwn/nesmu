#include "CPU.hpp"
#include <iostream>
#include <bit>

CPU6502::CPU6502() : PC(0x0), SP(0x0), PS(0x0), A(0x0), X(0x0), Y(0x0){}
CPU6502::~CPU6502() = default;
void CPU6502::reset(Memory& mem) {	//reset 6502 to initial state
	this->PC = 0xfffc;
	this->SP = 0x0100;
	this->PS = 0x0;		//clear all flags for now
	this->A = 0x0;
	this->X = 0x2;
	this->Y = 0x0;
	mem.initialize();
}

Word fixEndian(Word w) {
    if constexpr (std::endian::native == std::endian::big) {
        Word leftPart = w & 0xff00;
        Word rightPart = w & 0x00ff;
        w = (leftPart >> 8) | (rightPart << 8);
    }
    return w;
}

Byte CPU6502::fetchByte(Memory& mem, u32& cycles) {
	Byte d = mem[PC];
	PC++;
	cycles--;
	return d;
}

Word CPU6502::fetchWord(Memory& mem, u32& cycles) {
    //6502 is *little* endian
    Word d = mem[PC];
    PC++;
    cycles--;
    d = d | (mem[PC] << 8);
    PC++;
    cycles--;

    //if you want to handle endianness
    //you would have to swap bytes here
    //if(platform_big_endian)
    //  swap_bytes_in_word
    d = fixEndian(d);
    return d;
}

Byte CPU6502::readByte(Memory& mem, Word address, u32& cycles) {
    Byte d = mem[address];
    cycles--;
    return d;
}

Word CPU6502::readWord(Memory &mem, Word address, u32 &cycles) {
    //6502 is *little* endian
    Word d = mem[address];
    cycles--;
    d = d | (mem[address+1] << 8);
    cycles--;

    //if you want to handle endianness
    //you would have to swap bytes here
    //if(platform_big_endian)
    //  swap_bytes_in_word
    d = fixEndian(d);
    return d;
}

void CPU6502::writeByte(Memory& mem, Word address, u32& cycles, Byte data) {
    mem[address] = data;
    cycles--;
}

void CPU6502::writeWord(Memory &mem, Word address, u32 &cycles, Byte data) {
    std::cout << "writeWord IS UNIMPLEMENTED!!!" << std::endl;
}

void CPU6502::LDASetStatus() {
    if(A == 0) {
        PS = PS | 0b00000010;
    }
    if((A & 0b10000000) > 0) {
        PS = PS | 0b10000000;
    }
}

void CPU6502::LDXSetStatus() {
    if(X == 0) {
        PS = PS | 0b00000010;
    }
    if((X & 0b10000000) > 0) {
        PS = PS | 0b10000000;
    }
}

void CPU6502::LDYSetStatus() {
    if(Y == 0) {
        PS = PS | 0b00000010;
    }
    if((Y & 0b10000000) > 0) {
        PS = PS | 0b10000000;
    }
}

void CPU6502::ANDSetStatus() {
    if(A == 0) {
        PS = PS | 0b00000010;
    }
    if(A & 0b10000000) {
        PS = PS | 0b10000000;
    }
}

void CPU6502::execute(Memory& mem, u32 cycles) {
	while(cycles > 0) {
		Byte ins = fetchByte(mem, cycles);
		switch(ins) {
		    case INS_LDA_IM: {
                Byte val = fetchByte(mem, cycles);
                A = val;
               LDASetStatus();
		    } break;
		    case INS_LDA_ZP: {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                A = readByte(mem, zeroPageAddress, cycles);
                LDASetStatus();
		    } break;
            case INS_LDA_ZPX: {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                zeroPageAddress += X;
                cycles--;
                A = readByte(mem, zeroPageAddress, cycles);
                LDASetStatus();
            } break;
            case INS_LDA_AB: {
                Word addr = fetchWord(mem, cycles);
                A = readByte(mem, addr, cycles);
                LDASetStatus();
            } break;
            case INS_LDA_ABX: {
                Word addr = fetchWord(mem, cycles);
                A = readByte(mem, addr+X, cycles);
                if((addr + X) % 256 < addr % 256) {
                    //page crossed
                    cycles--;
                }
                LDASetStatus();
            } break;
            case INS_LDA_ABY: {
                Word addr = fetchWord(mem, cycles);
                A = readByte(mem, addr+Y, cycles);
                if((addr + Y) % 256 < addr % 256) {
                    //page crossed
                    cycles--;
                }
                LDASetStatus();
            } break;
            case INS_LDA_IxIdX: {
                Byte addr = fetchByte(mem, cycles); //1 cycle
                addr += X;
                cycles--;
                Word realAddr = readWord(mem, addr, cycles); //2 cycles
                A = readByte(mem, realAddr, cycles); // 1 cycle
                // -> 6 cycles
                LDASetStatus();
            } break;
            case INS_LDA_IdIxY: {
                Byte addr = fetchByte(mem, cycles); //1 cycle
                Word realAddr = readWord(mem, addr, cycles); //2 cycles
                if(addr == 0xFF) { //-> the 2nd byte of the real address is in next page
                    cycles--;
                }
                A = readByte(mem, realAddr+Y, cycles); // 1 cycle
                // -> 5(+1 if next page) cycles
                LDASetStatus();
            } break;
            case INS_LDX_IM: {
                Byte val = fetchByte(mem, cycles);
                X = val;
                LDXSetStatus();
            } break;
            case INS_LDX_ZP: {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                X = readByte(mem, zeroPageAddress, cycles);
                LDXSetStatus();
            } break;
            case INS_LDX_ZPY:  {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                zeroPageAddress += Y;
                cycles--;
                X = readByte(mem, zeroPageAddress, cycles);
                LDXSetStatus();
            } break;
            case INS_LDX_AB: {
                Word addr = fetchWord(mem, cycles);
                X= readByte(mem, addr, cycles);
                LDXSetStatus();
            } break;
            case INS_LDX_ABY: {
                Word addr = fetchWord(mem, cycles);
                X = readByte(mem, addr+Y, cycles);
                if((addr + Y) % 256 < addr % 256) {
                    //page crossed
                    cycles--;
                }
                LDXSetStatus();
            } break;
            case INS_LDY_IM: {
                Byte val = fetchByte(mem, cycles);
                Y = val;
                LDYSetStatus();
            } break;
            case INS_LDY_ZP: {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                Y= readByte(mem, zeroPageAddress, cycles);
                LDYSetStatus();
            } break;
            case INS_LDY_ZPX:  {
                Byte zeroPageAddress = fetchByte(mem, cycles);
                zeroPageAddress += X;
                cycles--;
                Y = readByte(mem, zeroPageAddress, cycles);
                LDYSetStatus();
            } break;
            case INS_LDY_AB: {
                Word addr = fetchWord(mem, cycles);
                Y = readByte(mem, addr, cycles);
                LDYSetStatus();
            } break;
            case INS_LDY_ABX: {
                Word addr = fetchWord(mem, cycles);
                Y = readByte(mem, addr+X, cycles);
                if((addr + X) % 256 < addr % 256) {
                    //page crossed
                    cycles--;
                }
                LDYSetStatus();
            } break;
            case INS_STA_ZP: {
                Byte addr = fetchByte(mem, cycles);
                writeByte(mem, addr, cycles, A);
            } break;
            case INS_STA_ZPX: {
                Byte addr = fetchByte(mem, cycles);
                addr += X;
                cycles--;
                writeByte(mem, addr, cycles, A);
            } break;
            case INS_STA_AB: {
                Word addr = fetchWord(mem, cycles);
                writeByte(mem, addr, cycles, A);
            } break;
            case INS_STA_ABX: {
                Word addr = fetchWord(mem, cycles);
                addr += X;
                cycles--;
                writeByte(mem, addr, cycles, A);
            } break;
            case INS_STA_ABY: {
                Word addr = fetchWord(mem, cycles);
                addr += Y;
                cycles--;
                writeByte(mem, addr, cycles, A);
            } break;
            case INS_STA_IxIdX: {
                Byte addr = fetchByte(mem, cycles); //-1
                addr += X;
                cycles--; //-1
                Word realAddr = readWord(mem, addr, cycles); //-2
                writeByte(mem, realAddr, cycles, A);
            } break;
            case INS_STA_IdIxY: {
                Byte addr = fetchByte(mem, cycles);
                Word realAddr = readWord(mem, addr, cycles);
                realAddr +=  Y;
                cycles--;
                writeByte(mem, realAddr, cycles, A);
            } break;
            case INS_STX_ZP: {
                Byte addr = fetchByte(mem, cycles);
                writeByte(mem, addr, cycles, X);
            } break;
            case INS_STX_ZPY: {
                Byte addr = fetchByte(mem, cycles);
                addr += Y;
                cycles--;
                writeByte(mem, addr, cycles, X);
            } break;
            case INS_STX_AB: {
                Word addr = fetchWord(mem, cycles);
                writeByte(mem, addr, cycles, X);
            } break;
            case INS_STY_ZP: {
                Byte addr = fetchByte(mem, cycles);
                writeByte(mem, addr, cycles, Y);
            } break;
            case INS_STY_ZPX: {
                Byte addr = fetchByte(mem, cycles);
                addr += X;
                cycles--;
                writeByte(mem, addr, cycles, Y);
            } break;
            case INS_STY_AB: {
                Word addr = fetchWord(mem, cycles);
                writeByte(mem, addr, cycles, Y);
            } break;
            case INS_JSR: {
                Word subAddr = fetchWord(mem, cycles);
                mem.writeWord(PC - 1, SP, cycles);
                SP++;
                PC = subAddr;
                cycles--;
            } break;
            case INS_TAX: {
                X = A;
                cycles--;
                if(X == 0) {
                    PS = PS | 0b00000010;
                }else if((X & 0b10000000) > 0) {
                    PS = PS | 0b10000000;
                }
            } break;
            case INS_TAY: {
                Y = A;
                cycles--;
                if(Y == 0) {
                    PS = PS | 0b00000010;
                }else if((Y & 0b10000000) > 0) {
                    PS = PS | 0b10000000;
                }
            } break;
            case INS_TXA: {
                A = X;
                cycles--;
                if(A == 0) {
                    PS = PS | 0b00000010;
                }else if((A & 0b10000000) > 0) {
                    PS = PS | 0b10000000;
                }
            } break;
            case INS_TYA: {
                A = Y;
                cycles--;
                if(A == 0) {
                    PS = PS | 0b00000010;
                }else if((A & 0b10000000) > 0) {
                    PS = PS | 0b10000000;
                }
            } break;
            case INS_PHA: {
                writeByte(mem, SP, cycles, A);
                SP++;
                cycles--;
            } break;
            case INS_PHP: {
                writeByte(mem, SP, cycles, PS);
                SP++;
                cycles--;
            } break;
            case INS_PLA: {
                Byte dat = readByte(mem, (SP-1), cycles);
                A = dat;
                cycles--;
                if(A == 0) {
                    PS = PS | 0b00000010;
                } else if((A & 0b10000000) > 0) {
                    PS = PS | 0b10000000;
                }
                SP--;
                cycles--;
            } break;
            case INS_PLP: {
                Byte dat = readByte(mem, (SP-1), cycles);
                PS = dat;
                cycles--;
                SP--;
                cycles--;
            } break;
            case INS_AND_IM: {
                Byte val = fetchByte(mem, cycles);
                A = A & val;
                ANDSetStatus();
            } break;
            case INS_AND_ZP: {
                Byte val = fetchByte(mem, cycles);
                Byte zpval = readByte(mem, val, cycles);
                A = A & zpval;
                ANDSetStatus();
            } break;
            case INS_AND_ZPX: {
                Byte val = fetchByte(mem, cycles);
                Word added = val;
                added += X;
                cycles--;
                Byte zpval = readByte(mem, added, cycles);
                A = A & zpval;
                ANDSetStatus();
            } break;
            case INS_AND_AB: {
                Word val = fetchWord(mem, cycles);
                Byte v = readByte(mem, val, cycles);
                A = A & v;
                ANDSetStatus();
            } break;
            case INS_AND_ABX: {
                Word val = fetchWord(mem, cycles);
                Word rem1 = val % 255;
                val += X;
                Word rem2 = val % 255;
                if(rem1 > rem2) {
                    cycles--;
                }
                Byte v = readByte(mem, val, cycles);
                A = A & v;
                ANDSetStatus();
            } break;
            case INS_AND_ABY: {
                Word val = fetchWord(mem, cycles);
                Word rem1 = val % 255;
                val += Y;
                Word rem2 = val % 255;
                if(rem1 > rem2) {
                    cycles--;
                }
                Byte v = readByte(mem, val, cycles);
                A = A & v;
                ANDSetStatus();
            } break;
            case INS_AND_IxIdX: {
                Byte addr = fetchByte(mem, cycles); //-1
                addr += X;
                cycles--; //-1
                Word realAddr = readWord(mem, addr, cycles); //-2
                Byte val = readByte(mem, realAddr, cycles);
                A = A & val;
                ANDSetStatus();
            } break;
            case INS_AND_IdIxY: {
                Byte addr = fetchByte(mem, cycles);
                Word realAddr = readWord(mem, addr, cycles);
                Word rem1 = realAddr % 255;
                Word rem2 = (realAddr+Y) % 255;
                Byte v = readByte(mem, realAddr+Y, cycles);
                if(rem1 > rem2) {
                    cycles--;
                }
            } break;
		    default:
                std::cout << "Unhandled instruction: " << ins << std::endl;
		        break;
		}
	}	
}


void Memory::initialize() {
	for(Byte& i : data) {
		i = 0;
	}
}

void Memory::writeWord(Word val, u32 addr, u32& cycles) {
    data[addr] = val & 0xff;
    data[addr + 1] = (val >> 8);
    cycles -= 2;
}
