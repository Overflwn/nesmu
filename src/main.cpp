#include <iostream>
#include "CPU.hpp"

int main(int argc, char** argv) {
	std::cout << "Hello, NESmu!" << std::endl;
	Memory mem;
	CPU6502 cpu;
	cpu.reset(mem);
	//test inline program
	mem[0xfffc] = CPU6502::INS_JSR;
	mem[0xfffd] = 0x00;
	mem[0xfffe] = 0x02;
    cpu.A = 69;
	mem[0x200] = CPU6502::INS_STA_ZP;
	mem[0x201] = 0x02;
	mem[0x202] = CPU6502::INS_LDX_ZPY;
	cpu.Y = 1;
	mem[0x203] = 0x01;

	cpu.execute(mem, 13);
	return 0;
}
